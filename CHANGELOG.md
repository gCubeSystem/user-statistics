
# Changelog for User Statistics portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v2.3.1] - 2021-10-28

- Removed HomeLibrary
- Updated GWT version to latest 

## [v2.3.0] - 2021-02-02

- Ported to git

- Feature #21008: UserStatistics portlet: Remove number of comments done and got indicator

## [v2.2.0] - 2017-02-20

- Added storage Quota information

## [v2.1.0] - 2016-10-02

- Posts related to some statistics are now retrievable on the demand

## [v2.0.0] - 2016-04-02

- Ported to Liferay 6.2

- Minor bug fix: long vre's name managed properly(Bug #2042)


## [v1.0.0] - 2015-12-09

- First release
