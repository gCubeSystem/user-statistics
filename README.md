# gCube System - User Statistics Portlet

This component is a Liferay 6.2.6 CE Porlet which interacts with workspace and social service to gather usage statistics (e.g. disk space in use, number of posts etc.)

## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* No Documentation is provided

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/VREFolder-hook/releases).

## Authors

* **Costantino Perciante** - [ISTI-CNR] former associate

## Maintainers

* **Massimiliano Assante** - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

    